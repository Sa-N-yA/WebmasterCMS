<ul class="pagination">
    <li{!!($result['current']!=1)?'':' class="disabled"'!!}><a href="{{url('?page='.($result['current']-1))}}">&laquo;</a></li>
    @for($i = 1; $i<=$result['count']; $i++)
        <li{!!($result['current']!=$i)?'':' class="active"'!!}><a href="{{url('?page='.$i)}}">{{$i}}</a></li>
    @endfor
    <li{!!($result['current']<$result['count'])?'':' class="disabled"'!!}><a href="{{url('?page='.($result['current']+1))}}">&raquo;</a></li>
</ul>