<div style="position: relative;border:1px solid green">
    <div class="component-debug-btn component-debug-btn-lt"></div>
    <div class="component-debug-btn component-debug-btn-lb"></div>
    <div class="component-debug-btn component-debug-btn-rt"></div>
    <div class="component-debug-btn component-debug-btn-rb"></div>
    <div class="component-debug-info">
        Название компонента: {{$componentName}}<br/>
        Шаблон: {{$view_name}}<br/>
        Время работы компонента: {{$time}}
    </div>
    @include($view_name, $result)
</div>