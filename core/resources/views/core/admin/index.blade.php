@extends('core.admin.layout')

@section('styles')
    <style>
        nav.navbar{
            margin-bottom: 0;
        }
        body:after{
            background: url(/admin/common/img/background.jpg);
            background-size: cover;
            width: 100%;
            height: 100%;
            content: '';
            position: absolute;
        }
        body{
            overflow: hidden;
        }
    </style>
@endsection