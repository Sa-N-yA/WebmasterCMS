<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel - Авторизация</title>

    <script src="/admin/common/js/jquery.js"></script>
    <script src="/admin/common/js/bootstrap.js"></script>
    <script src="/admin/common/js/main.js"></script>
    <script src="/admin/common/js/auth.js"></script>
    <link rel="stylesheet" href="http://bootstrap-3.ru/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/admin/common/css/main.css">

    <script>
        var csrf = '{{csrf_token()}}';
    </script>
</head>
<body>
<div class="panel panel-default" id="authorise-main">
    <div class="panel-heading">
        <h3>Laravel - Авторизация</h3>
    </div>
    <div class="panel-body">
        <form role="form" class="admin-form-ajax" id="authorise-form" data-action="User/auth">
            <div id="email" class="form-group">
                <label for="email">E-mail:</label>
                <input type="email" class="form-control" name="email"/>
            </div>
            <div class="form-group">
                <label for="password">Пароль:</label>
                <input type="password" class="form-control" name="password"/>
            </div>
            <div class="form-group col-md-6">
                <label for="email">Запомнить меня</label>
                <input type="hidden" name="remember" value="0"/>
                <input type="checkbox" name="remember" value="1"/>
            </div>
            <div class="form-group col-md-6">
                <input type="submit" class="btn btn-success pull-right" value="Войти"/>
            </div>
        </form>
    </div>
</div>
</body>
</html>