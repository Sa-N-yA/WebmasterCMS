@extends('core.admin.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Список гейтов</div>
        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th style="width: 5%;">#</th>
                    <th style="width: 22%;">Название</th>
                    <th style="width: 5%;">Системная</th>
                    <th style="width: 18%">Дата создания</th>
                    <th style="width: 10%"></th>
                </tr>
                </thead>
                @foreach($gates as $gate)
                    <tr>
                        <td>{{$gate->id}}</td>
                        <td>
                            @if($gate->system=='Y')
                                {{$gate->name}}
                            @else
                                <a href="/admin/users/gates/{{$group->id}}/update">{{$gate->name}}</a>
                            @endif
                        </td>
                        <td>{{$gate->system=='Y'?'Да':'Нет'}}</td>
                        <td>{{$gate->created_at}}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle"{{$gate->system=='Y'?' disabled':''}} data-toggle="dropdown">Действия <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/admin/users/gates/{{$gate->id}}/update">Изменить</a></li>
                                    <li><a href="/admin/users/{{$gate->id}}/update">Удалить</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="panel-footer">
            <a href="/admin/users/gates/create" class="btn btn-default">Создать</a>
        </div>
    </div>
    <script>
        $(function(){
            admin.registerButtonAjax($('.admin-btn-ajax'),function(){
                $(this).parents('tr').remove();
            });
        });
    </script>
@endsection