@extends('core.admin.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Список пользователей</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width: 5%;">#</th>
                        <th style="width: 15%;">ФИО</th>
                        <th style="width: 22%;">E-mail</th>
                        <th style="width: 15%">Дата создания</th>
                        <th style="width: 13%"></th>
                    </tr>
                </thead>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td><a href="/admin/users/{{$user->id}}/update">{{$user->name}}</a></td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->created_at}}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">Действия <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/admin/users/{{$user->id}}/update">Изменить</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="panel-footer">
            <a href="/admin/users/create" class="btn btn-default">Создать</a>
        </div>
    </div>
    <script>
        $(function(){
            admin.registerButtonAjax($('.admin-btn-ajax'),function(){
                $(this).parents('tr').remove();
            });
        });
    </script>
@endsection