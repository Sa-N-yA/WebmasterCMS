<div class="modal fade" id="modal-files" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close modal-close" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Файловый менеджер</h4>
            </div>
            <div class="modal-body">
                <ul class="nav nav-pills">
                    <li style="width: 100%;margin: 0;">
                        <form data-action="Files/create_folder" style="padding-left: 0;" class="navbar-form admin-form-ajax navbar-left col-md-6" >
                            <input type="hidden" name="parent_id">
                            <div id="name" class="form-group">
                                <input name="name" type="text" class="form-control" placeholder="Название папки">
                            </div>
                            <button type="submit" class="btn btn-default">Создать папку</button>
                        </form>
                        <div class="navbar-right navbar-form col-md-6">
                            <button type="button" class="modal-load-file btn btn-default">Загрузить новый файл</button>
                        </div>
                    </li>
                </ul>
                <ol class="breadcrumb">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Библиотека</a></li>
                    <li class="active">Данные</li>
                </ol>
                <div class="modal-list-files"></div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default modal-close">Закрыть</button>
                <button type="button" class="btn btn-primary modal-success modal-sections-select" data-id="0">Выбрать</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    function onRegisterModalFiles(){
        var currentFolder = 0;
        var self = this;
        self.folders = {
            0:{
                id: 0,
                name: 'Файловый менеджер'
            }
        };
        self.buildPath = function(folder_id){
            var path = [];
            var folder = self.folders[folder_id];
            if (folder_id!=0){
                path = self.buildPath(folder.parent_id);
            }
            path.push({name:folder.name, id: folder.id});
            return path;
        };
        self.loadFolder = function(folder_id){
            console.log(self.folders);
            if (self.folders[folder_id].children!=undefined){
                self.clearItems();
                if (folder_id!=0){
                    self.addItem({type:'back',id: self.folders[folder_id].parent_id});
                }
                var item;
                for(var i = 0; i<self.folders[folder_id].children.length; i++){
                    item = self.folders[folder_id].children[i];
                    self.addItem(item);
                }
                self.find('input[name=parent_id]').val(folder_id);
                self.find('.breadcrumb').html("");
                var path = self.buildPath(folder_id);
                for(var i = 0; i<path.length; i++){
                    if (i==path.length-1){
                        $('#modal-files .breadcrumb').append('<li class="active">' + path[i].name +'</li>')
                    }   else    {
                        $('#modal-files .breadcrumb').append('<li><a class="modal-item-folder" data-id="' + path[i].id +'" href="#">' + path[i].name +'</a></li>')
                    }
                }
                currentFolder = folder_id;
                return;
            }
            admin.ajax('Files/get_items',{folder_id:folder_id}, function(res){
                self.folders[folder_id].children = [];
                for(var i = 0; i<res['DATA'].length; i++){
                    var f = res['DATA'][i];
                    if (f.type=='folder') self.folders[f.id] = f;
                    self.folders[folder_id].children.push(f);
                }
                self.loadFolder(folder_id);
            });
        };
        self.clearItems = function(){
            self.find('.modal-list-files').html("");
        };
        self.addItem = function(item){
            if (item.type=='back'){
                self.find('.modal-list-files').append('' +
                    '<div class="modal-item modal-item-folder" data-id="' + item.id +'">' +
                        '<div class="modal-item-icon-back"></div>'+
                        '<div class="modal-item-name">Назад</div>'+
                    '</div>');
            }   else if (item.type=='folder'){
                $('<div class="modal-item modal-item-folder" data-id="' + item.id +'">' +
                    '<div class="modal-item-icon-folder"></div>'+
                    '<div class="modal-item-name">' + item.name + '</div>'+
                '</div>').appendTo(self.find('.modal-list-files')).popover({
                    delay:250,
                    placement: 'auto top',
                    title:item.name,
                    html:true,
                    content:"<p>Папок: " + item.count_folders + "</p><p>Файлов: " + item.count_files + "</p>",
                    trigger:'hover'
                });
            }   else if (item.type=='file'){
                $('<div class="modal-item modal-item-file modal-success" data-src="' + item.src_orig + '" data-name="' + item.name + '" data-id="' + item.id +'">' +
                        '<div class="modal-item-icon-file" style="background-image: url(\'' + item.src +'\')"></div>'+
                        '<div class="modal-item-name">' + item.name + '</div>'+
                        '</div>').appendTo(self.find('.modal-list-files')).popover({
                    delay:250,
                    placement: 'auto top',
                    title:item.name,
                    html:true,
                    content:"<p>Размер файла: " + item.filesize + " байт</p>",
                    trigger:'hover'
                });
            }
        };
        $('#modal-files').on('click','.modal-item-folder',function(){
            self.loadFolder($(this).attr('data-id'));
        });
        admin.registerFormAjax(self.find('.admin-form-ajax'),function(res){
            res['DATA'].type = 'folder';
            res['DATA'].count_folders = 0;
            res['DATA'].count_files = 0;
            res['DATA'].children = [];
            self.folders[currentFolder].children.push(res['DATA']);
            self.folders[res['DATA'].parent_id].count_folders++;
            self.folders[res['DATA'].id] = res['DATA'];
            self.loadFolder(currentFolder);
            self.find('input[type=text]').val("");
        },function(){
        });
        self.find('.modal-load-file').click(function(){
            var form = $('<form><input type="file" multiple name="files"/><input type="hidden" name="folder_id" value="' + currentFolder +'"></form>');
            form.submit(function(){
                admin.showModal('load-files',this);
                return false;
            });
            form.find('input[type=file]').change(function(){$(this).parent().submit();}).click();
        });
        admin.registerModal('load-files',onRegisterModalLoadFiles,function(data) {
            this.startLoad(data);
        }, function(elem){
        }, function(){
            delete(self.folders[currentFolder].children);
            self.loadFolder(currentFolder);
        });
    }
</script>