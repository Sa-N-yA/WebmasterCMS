<div class="modal fade" id="modal-sections" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close modal-close" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Выбор раздела инфоблока</h4>
            </div>
            <div class="modal-body">
                <table class="table table-collsapsed">
                    <tr>
                        <th class="col-md-1">#</th>
                        <th class="col-md-7">Название</th>
                        <th class="col-md-2">Псевдоним</th>
                        <th class="col-md-2"></th>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default modal-close">Закрыть</button>
                <button type="button" class="btn btn-primary modal-success modal-sections-select" data-id="0">Выбрать</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    var onRegisterModalSections = function(){
        this.sections = {};
        this.loadSections = function (section_id) {
            var self = this;
            self.sections[0] = "Корневой раздел";
            admin.ajax('IBlock/get_sections', {
                iblock_id: iblock_id,
                filter: [['parent_id', section_id]],
                fields: ['name', 'alias']
            }, function (res) {
                $('.modal-sections-select').attr('data-id',section_id);
                var table = self.find('table');
                table.find('tr.modal-sections').remove();
                if (section_id!=0) {
                    table.append('<tr class="modal-sections">' +
                            '<td></td>' +
                            '<td><a data-id="' + self.currentSection + '" href="#"><strong>...</strong></a></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '</tr>');
                }
                self.currentSection = section_id;
                for (var r in res['DATA']['items']) {
                    var r = res['DATA']['items'][r];
                    self.sections[r.id] = r.name;
                    table.append('<tr class="modal-sections">' +
                        '<td>' + r.id + '</td>' +
                        '<td><a data-id="' + r.id + '" href="#">' + r.name + '</a></td>' +
                        '<td>' + r.alias + '</td>' +
                        '<td><button data-id="' + r.id + '" class="btn btn-default modal-success btn-xs">Выбрать</button></td>' +
                        '</tr>');
                }
                table.find('a').click(function () {
                    self.loadSections($(this).attr('data-id'));
                    return false;
                });
            });
        };
    }
</script>