@extends('core.admin.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Список инфоблоков</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width: 5%;">#</th>
                        <th style="width: 27%;">Название</th>
                        <th style="width: 10%;">Псевдоним</th>
                        <th style="width: 15%;">Количество разделов</th>
                        <th style="width: 15%">Количество элементов</th>
                        <th style="width: 13%"></th>
                    </tr>
                </thead>
                @foreach($iblocks as $iblock)
                    <tr>
                        <td>{{$iblock->id}}</td>
                        <td><a href="/admin/iblock/{{$iblock->id}}">{{$iblock->name}}</a></td>
                        <td>{{$iblock->alias}}</td>
                        <td>{{$iblock->sections->count()}}</td>
                        <td>{{$iblock->items->count()}}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">Действия <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/admin/iblock/{{$iblock->id}}/create_section">Создать раздел</a></li>
                                    <li><a href="/admin/iblock/{{$iblock->id}}/create_prop_section">Создать свойство раздела</a></li>
                                    <li><a href="/admin/iblock/{{$iblock->id}}/list_props_section">Список свойств раздела</a></li>
                                    <li class="divider"></li>
                                    <li><a href="/admin/iblock/{{$iblock->id}}/create_item">Создать элемент</a></li>
                                    <li><a href="/admin/iblock/{{$iblock->id}}/create_prop_item">Создать свойство элемента</a></li>
                                    <li><a href="/admin/iblock/{{$iblock->id}}/list_props_item">Список свойств элементов</a></li>
                                    <li class="divider"></li>
                                    <li><a href="/admin/iblock/{{$iblock->id}}/update">Изменить</a></li>
                                    <li><a href="#" class="admin-btn-ajax" data-method="IBlock/remove_iblock" data-id="{{$iblock->id}}">Удалить</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="panel-footer">
            <a href="/admin/iblock/create" class="btn btn-default">Создать</a>
        </div>
    </div>
    <script>
        $(function(){
            admin.registerButtonAjax($('.admin-btn-ajax'),function(){
                $(this).parents('tr').remove();
            });
        });
    </script>
@endsection