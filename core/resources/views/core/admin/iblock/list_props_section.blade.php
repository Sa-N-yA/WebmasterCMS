@extends('core.admin.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Список свойств разделов инфоблока {{$iblock->name}}</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width: 5%;">#</th>
                        <th style="width: 27%;">Название</th>
                        <th style="width: 10%;">Псевдоним</th>
                        <th style="width: 10%;">Обязательное</th>
                        <th style="width: 10%;">Множественное</th>
                        <th style="width: 15%;">Тип</th>
                        <th style="width: 13%"></th>
                    </tr>
                </thead>
                @foreach($props as $prop)
                    <tr>
                        <td>{{$prop->id}}</td>
                        <td>{{$prop->name}}</td>
                        <td>{{$prop->alias}}</td>
                        <td>{{($prop->required)?'Да':'Нет'}}</td>
                        <td>{{($prop->many)?'Да':'Нет'}}</td>
                        <td>{{$prop->typeName}}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">Действия <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/admin/iblock/{{$prop->iblock_id}}/props_section/{{$prop->id}}/update">Изменить</a></li>
                                    <li><a href="#" class="admin-btn-ajax" data-method="IBlock/remove_section_prop" data-id="{{$prop->id}}">Удалить</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
            {{$props->render()}}
        </div>
        <div class="panel-footer">
            <a href="/admin/iblock/{{$iblock->id}}/create_prop_section" class="btn btn-default">Создать новое свойство</a>
        </div>
    </div>
    <script>
        $(function(){
            admin.registerButtonAjax($('.admin-btn-ajax'),function(){
                $(this).parents('tr').remove();
            });
        });
    </script>
@endsection