@extends('core.admin.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">ФИЛЬТР</div>
        </div>
        <form action="" method="get">
            <div class="panel-body">
                <input type="hidden" name="filter" value="Y"/>
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Тип</label>
                    <select name="type" class="form-control">
                        <option value="all"{{@$filter['type']=='all'?' selected':''}}>Разделы и элементы</option>
                        <option value="sections"{{@$filter['type']=='sections'?' selected':''}}>Только разделы</option>
                        <option value="items"{{@$filter['type']=='items'?' selected':''}}>Только элементы</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Активность</label>
                    <select name="active" class="form-control">
                        <option value="all"{{@$filter['active']=='all'?' selected':''}}>Все</option>
                        <option value="y"{{@$filter['active']=='y'?' selected':''}}>Только активные</option>
                        <option value="n"{{@$filter['active']=='n'?' selected':''}}>Только неактивные</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">ID</label>
                    <input type="number" name="id" class="form-control" value="{{@$filter['id']}}">
                </div>
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Название содержит</label>
                    <input type="text" name="name" class="form-control" value="{{@$filter['name']}}">
                </div>
            </div>
            <div class="panel-footer">
                <input type="submit" class="btn btn-primary" value="Применить">
                <a href="?filter=N" class="btn btn-default">Сбросить</a>
            </div>
        </form>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Инфоблок</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th style="width: 5%;"></th>
                    <th style="width: 5%;">#</th>
                    <th style="width: 27%;">Название</th>
                    <th style="width: 10%;">Псевдоним</th>
                    <th style="width: 15%;">Дата создания</th>
                    <th style="width: 13%"></th>
                </tr>
                </thead>
                @if($section!=null)
                    <tr>
                        <td></td>
                        <td></td>
                        <td><a href="/admin/iblock/{{$iblock->id}}/{{$section->parent_id}}"><strong>...</strong></a></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                @endif
                @foreach($sections['items'] as $section)
                    <tr{!!$section['active']=='N'?' class="danger"':''!!}>
                        <td class="admin-icon-iblock-section"></td>
                        <td>{{$section['id']}}</td>
                        <td><a href="/admin/iblock/{{$iblock->id}}/{{$section['id']}}">{{$section['name']}}</a></td>
                        <td>{{$section['alias']}}</td>
                        <td>{{$section['created_at']}}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">Действия <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/admin/iblock/{{$iblock->id}}/create_section">Создать раздел</a></li>
                                    <li class="divider"></li>
                                    <li><a href="/admin/iblock/{{$iblock->id}}/create_item">Создать элемент</a></li>
                                    <li class="divider"></li>
                                    <li><a href="/admin/iblock/{{$iblock->id}}/sections/{{$section['id']}}/update" >Изменить</a></li>
                                    <li><a href="/admin/iblock/{{$iblock->id}}/sections/{{$section['id']}}/copy" >Копировать</a></li>
                                    <li><a href="#" class="deactivate admin-btn-ajax" data-method="IBlock/deactivate_section" data-id="{{$section['id']}}">Деактивировать</a></li>
                                    <li><a href="#" class="restore admin-btn-ajax" data-method="IBlock/restore_section" data-id="{{$section['id']}}">Восстановить</a></li>
                                    <li><a href="#" class="remove admin-btn-ajax" data-method="IBlock/remove_section" data-id="{{$section['id']}}">Удалить</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                @foreach($items['items'] as $item)
                    <tr{!!$item['active']=='N'?' class="danger"':''!!}>
                        <td class="admin-icon-iblock-item"></td>
                        <td>{{$item['id']}}</td>
                        <td><a href="/admin/iblock/{{$iblock->id}}/items/{{$item['id']}}/update">{{$item['name']}}</a></td>
                        <td>{{$item['alias']}}</td>
                        <td>{{$item['created_at']}}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">Действия <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/admin/iblock/{{$iblock->id}}/items/{{$item['id']}}/update" >Изменить</a></li>
                                    <li><a href="/admin/iblock/{{$iblock->id}}/items/{{$item['id']}}/copy" >Копировать</a></li>
                                    <li><a class="deactivate admin-btn-ajax" data-method="IBlock/deactivate_item" data-id="{{$item['id']}}" href="/admin/iblock/{{$iblock->id}}/create_item">Деактивировать</a></li>
                                    <li><a class="restore admin-btn-ajax" data-method="IBlock/restore_item" data-id="{{$item['id']}}" href="/admin/iblock/{{$iblock->id}}/create_item">Восстановить</a></li>
                                    <li><a class="remove admin-btn-ajax" data-method="IBlock/remove_item" data-id="{{$item['id']}}" href="/admin/iblock/{{$iblock->id}}/create_item">Удалить</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="panel-footer">
            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Разделы <span class="caret"></span></button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="/admin/iblock/{{$iblock->id}}/create_section">Создать раздел</a></li>
                    <li><a href="/admin/iblock/{{$iblock->id}}/list_props_section">Список свойств разделов</a></li>
                    <li><a href="/admin/iblock/{{$iblock->id}}/create_prop_section">Создать свойство разделов</a></li>
                </ul>
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Элементы <span class="caret"></span></button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="/admin/iblock/{{$iblock->id}}/create_item">Создать элемент</a></li>
                    <li><a href="/admin/iblock/{{$iblock->id}}/list_props_item">Список свойств элементов</a></li>
                    <li><a href="/admin/iblock/{{$iblock->id}}/create_prop_item">Создать свойство элементов</a></li>
                </ul>
            </div>
        </div>
    </div>
    <script>
        $(function(){
            admin.registerButtonAjax($('.admin-btn-ajax.deactivate'),function(){
                $(this).parents('tr').addClass('danger');
            });
            admin.registerButtonAjax($('.admin-btn-ajax.restore'),function(){
                $(this).parents('tr').removeClass('danger');
            });
            admin.registerButtonAjax($('.admin-btn-ajax.remove'),function(){
                $(this).parents('tr').remove();
            });
        });
    </script>
@endsection
@section('styles')
    <style>
        tr .deactivate{
            display: block !important;
        }
        tr .restore, tr .remove{
            display: none !important;
        }
        tr.danger .restore, tr.danger .remove{
            display: block !important;
        }
        tr.danger .deactivate{
            display: none !important;
        }
    </style>
@endsection