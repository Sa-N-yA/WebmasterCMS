<div class="form-group">
    <label for="iblock_section_type" class="col-md-4 control-label">Инфоблок</label>
    <div id="iblock_section_type" class="col-md-6">
        <select class="form-control" name="iblock_section_type">
            @foreach(App\Http\Controllers\Core\Modules\IBlockFields\IBlockSectionFieldController::$types as $key=>$value)
                <option value="{{$key}}" {{isset($values['iblock_section_type']) && $values['iblock_section_type'] == $key?'selected':''}}>{{$value}}</option>
            @endforeach
        </select>
    </div>
</div>