@extends('core.admin.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Редактирование элемента</div>
        <div class="panel-body">
            <form class="form admin-form-ajax" id="create-section-form" role="form" method="POST" data-action="IBlock/update_item">
                <input type="hidden" name="iblock_id" value="{{$iblock->id}}">
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">ID</label>
                    <div id="name" class="col-md-6">
                        <input type="text" class="form-control" readonly value="{{$item['id']}}" name="id">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Название элемента</label>
                    <div id="name" class="col-md-6">
                        <input type="text" class="form-control" value="{{$item['name']}}" name="name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="alias" class="col-md-4 control-label">Псевдоним</label>
                    <div id="alias" class="col-md-6">
                        <input type="text" class="form-control" name="alias" value="{{$item['alias']}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="alias" class="col-md-4 control-label">Дата активности</label>
                    <div id="active_date" class="col-md-6">
                        <input type="text" class="form-control" name="active_from" value="{{$item['active_from']}}">
                        <input type="text" class="form-control" name="active_to" value="{{$item['active_to']}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="alias" class="col-md-4 control-label">Родительский раздел</label>
                    <div id="section" class="col-md-6">
                        <input type="hidden" name="parent_id" value="{{$section['id']}}"/>
                        <input style="width: 75%;float: left;" type="text" class="form-control" readonly value="{{$section['name']}}">
                        <button data-iblock-id="{{$iblock->id}}" type="button" style="margin-left: 5%;width: 20%;" class="btn btn-default section-select">Выбрать</button>
                    </div>
                </div>
                @foreach($props as $prop)
                    <div class="form-group">
                        <label for="alias" class="col-md-4 control-label">{{$prop->name}}</label>
                        <div id="prop_{{$prop->alias}}" class="col-md-6">
                            @if($prop->many && !in_array($prop->type,['visual']))
                                <div id="prop_{{$prop->alias}}_list">
                                    @for($i=1; $i<=max(3,count($item[$prop->alias])+2); $i++)
                                        @include($prop->class->getFormView(),[
                                            'value'=>isset($item[$prop->alias][$i-1])?$item[$prop->alias][$i-1]:'',
                                            'many'=>'_'.$i,
                                            'files'=>$files,
                                            'items'=>$prop_items,
                                            'sections'=>$prop_sections,
                                            'config'=>$prop->config
                                        ])
                                    @endfor
                                </div>
                                <button data-type="{{$prop->type}}" data-id="{{$prop->alias}}" type="button" class="btn btn-default btn-more">Ещё</button>
                            @else
                                @include($prop->class->getFormView(),[
                                    'value'=>$item[$prop->alias],
                                    'many'=>'',
                                    'files'=>$files,
                                    'items'=>$prop_items,
                                    'sections'=>$prop_sections,
                                    'config'=>$prop->config
                                ])
                            @endif
                        </div>
                    </div>
                @endforeach
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="button" class="btn btn-primary btn-submit">
                            Сохранить
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        var moreInputs = {
            @foreach($props as $prop)
                @if($prop->many)
                    {{$prop->alias}}: {{max(3,count($item[$prop->alias])+2)}},
                @endif
            @endforeach
        };
        var iblock_id = -1;
        $(function(){
            $('.btn-more').click(function(e){
                var id = $(this).attr('data-id');
                var elem = $('#prop_' + id + '_list');
                switch($(this).attr('data-type')){

                    @foreach($props as $prop)
                    case '{{$prop->type}}':
                        @include($prop->class->getManyView())
                                break;
                    @endforeach
                }
            });
            $('.btn-submit').click(function(){
                var ta = $('.admin-visual');
                for (var i = 0; i<ta.length; i++){
                    ta.eq(i).html(tinymce.get(ta.eq(i).attr('id')).getContent());
                }
                $(this).parents('form').submit();
            });
            var selectFileCallback;
            $('body').on('click','.file-select',function(){
                var self = this;
                selectFileCallback = function(elem){
                    $(self).prev().val($(elem).attr('data-name')).prev().val($(elem).attr('data-id'));
                };
                admin.showModal('files');
            });
            admin.registerFormAjax($('#create-section-form'),function(res){
            },function(){
            });
            tinymce.init({
                setup: function (editor) {
                    editor.addMenuItem('myitem', {
                        text: 'Open file manager',
                        context: 'tools',
                        onclick: function() {
                            selectFileCallback = function(elem){
                                editor.insertContent('<img src="' + $(elem).attr('data-src') +'"/>');
                            };
                            admin.showModal('files');
                        }
                    });
                },
                selector:'textarea.admin-visual'
            });
            var lastSectionSelect = null, lastItemSelect = null;
            $('body').on('click','.section-select',function(){
                iblock_id = $(this).attr('data-iblock-id');
                admin.showModal('sections', 0);
                lastSectionSelect = this;
            });
            $('body').on('click','.item-select',function(){
                iblock_id = $(this).attr('data-iblock-id');
                admin.showModal('items', 0);
                lastItemSelect = this;
            });
            admin.registerModal('sections',onRegisterModalSections,function() {
                this.loadSections(0);
            }, function(elem){
                if (lastSectionSelect!=null) {
                    $(lastSectionSelect).parent().find('[type=hidden]').val($(elem).attr('data-id'));
                    $(lastSectionSelect).parent().find('[type=text]').val(this.sections[$(elem).attr('data-id')]);
                }
                return true;
            });
            admin.registerModal('items',onRegisterModalItems,function() {
                this.loadSections(0);
            }, function(elem){
                if (lastItemSelect!=null) {
                    $(lastItemSelect).parent().find('[type=hidden]').val($(elem).attr('data-id'));
                    $(lastItemSelect).parent().find('[type=text]').val(this.sections[$(elem).attr('data-id')]);
                }
                return true;
            });
            admin.registerModal('files',onRegisterModalFiles,function() {
                this.loadFolder(0);
            }, function(elem){
                if (selectFileCallback) selectFileCallback(elem);
                return true;
            });
            admin.registerFormAjax($('#create-section-form'),function(res){
                location.href = "/admin/iblock/{{$iblock->id}}";
            },function(){
            });
        });
    </script>
@endsection
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    