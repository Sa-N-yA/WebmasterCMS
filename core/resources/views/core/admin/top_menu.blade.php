<ul class="nav navbar-nav">
    <li><a href="/admin/">Главная</a></li>
    @foreach($menu as $item)
        <li class="dropdown">
            @if(count($item['children'])>0)
            <a href="{{$item['href']}}" class="dropdown-toggle" data-toggle="dropdown">
                {{$item['text']}} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                @foreach($item['children'] as $subitem)
                    @if($subitem['text']!='-')
                        <li><a href="{{$subitem['href']}}">{{$subitem['text']}}</a></li>
                    @else
                        <li class="divider"></li>
                    @endif
                @endforeach
            </ul>
            @else
                <a href="{{$item['href']}}">
                    {{$item['text']}}
                </a>
            @endif
        </li>
    @endforeach
</ul>