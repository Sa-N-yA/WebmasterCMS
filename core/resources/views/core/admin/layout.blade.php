<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="http://bootstrap-3.ru/dist/js/bootstrap.min.js"></script>
    <script src="/admin/common/js/moment.js"></script>
    <script src="/admin/common/js/jquery.daterangepicker.min.js"></script>
    <script>
        var csrf = '{{csrf_token()}}';
    </script>
    <script src="/admin/common/js/main.js"></script>
    @if(isset($scripts))
        @foreach($scripts as $script)
            <script src="{{$script}}"></script>
        @endforeach
    @endif
    <link rel="stylesheet" href="http://bootstrap-3.ru/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/admin/common/css/daterangepicker.min.css">
    <link rel="stylesheet" href="/admin/common/css/main.css">
    @yield('styles')

</head>
<body id="app-layout">
@if(isset($modals))
    @foreach($modals as $modal)
        @include('core.admin.modals.'.$modal)
    @endforeach
@endif
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                Laravel
            </a>
        </div>
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                @include('core.admin.top_menu', ['menu'=>$top_menu])
            </ul>
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ url('/admin/logout') }}" class="btn brn"><i class="fa fa-btn fa-sign-out"></i>Выйти</a></li>
            </ul>
        </div>
    </div>
</nav>

@yield('content')

</body>
</html>