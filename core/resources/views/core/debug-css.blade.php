<style>
    .component-debug-btn{
        position: absolute;
        width: 10px;
        height: 10px;
        background: #ff0000;
    }
    .component-debug-btn-lt{
        left: 0;
        top: 0;
    }
    .component-debug-btn-lb{
        left: 0;
        bottom: 0;
    }
    .component-debug-btn-rt{
        right: 0;
        top: 0;
    }
    .component-debug-btn-rb{
        right: 0;
        bottom: 0;
    }
    .component-debug-info{
        position: absolute;
        background: #444444;
        border:1px solid #000;
        color:#ffffff;
        top: 0;
        left: 0;
        display:none;
    }
</style>