<?php

return [
    'versionName'=>'1.0.3',
    'version'=>3,
    'server'=>'http://cms.webmaster38.ru/',
    'modules'=>[
        'IBlock'=>[
            'enabled'=>true,
            'alias'=>'iblock',
            'name'=>'Информационные блоки',
        ],
        'Users'=>[
            'enabled'=>true,
            'alias'=>'users',
            'name'=>'Пользователи'
        ],
        'WebForms'=>[
            'enabled'=>true,
            'alias'=>'webforms',
            'name'=>'Веб-формы',
        ]
    ]
];
