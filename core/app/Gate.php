<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gate extends Model
{
    protected $table = 'gates';

    public function groups(){
        return $this->belongsToMany('App\\Group','gate_groups');
    }
}
