<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IBLockItemPropValue extends Model
{
    protected $table = "iblock_item_props_values";
    public $timestamps = false;

    public function prop(){
        return $this->hasOne("\\App\\IBlockItemProp", 'prop_id');
    }

    public function item(){
        return $this->belongsTo("\\App\\IBlockItem", 'item_id');
    }

}
