<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Controllers\Core\Modules\IBlockFields\IBLockFieldController;
class IBlockSection extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'iblock_sections';

    public function items(){
        return $this->hasMany('\\App\\IBlockItem', 'section_id');
    }

    public function iblock(){
        return $this->belongsTo('\\App\\IBlock', 'iblock_id');
    }

    /**
     * Пробуем сохранить раздел
     */
    public function save(array $options = Array()){
        //Валидаторы для основных свойств
        $validation = [];
        $validation['name'] = "required|min:3|max:255";
        $validation['alias'] = "required|min:3|max:255|unique:iblock_items,alias".(($this->id?','.$this->id:',NULL')).",id,iblock_id,".$this->iblock_id."|regex:([a-zA-Z][a-zA-Z0-9_]*)";
        $validation['parent_id'] = "required|numeric|min:0";
        $validation['iblock_id'] = "required|numeric|exists:iblock,id";
        if (!empty($this->active_from) && !empty($this->active_to)){
            $validation['active_from'] = "date_format:Y-m-d H:i:s";
            $validation['active_to'] = "date_format:Y-m-d H:i:s";
        }   else    {
            $this->active_from = null;
            $this->active_to = null;
        }
        $props = IBlockSectionProp::where('iblock_id',$this->iblock_id)->get();
        //Прибираемся во множественных свойствах
        foreach($props as $prop) {
            $props_values = [];
            if ($prop->many=='1'){
                $i = 1;
                while(true){
                    $prop_name = 'prop_'.$prop->alias.'_'.$i;
                    if (!isset($this->$prop_name)) break;
                    $i++;
                    if (empty($this->$prop_name) || (($prop->type=='iblock_item' || $prop->type=='iblock_section' || $prop->type=='file') && $this->$prop_name=='-1')){
                        unset($this->$prop_name);
                        continue;
                    }
                    $props_values[] = $this->$prop_name;
                    unset($this->$prop_name);
                }
                $i = 1;
                foreach ($props_values as $prop_value){
                    $prop_name = 'prop_'.$prop->alias.'_'.$i;
                    $this->$prop_name = $prop_value;
                    $i++;
                }
            }
        }

        //Делаем валидаторы для пользовательских свойств
        foreach($props as $prop){
            $prop_name = 'prop_'.$prop->alias;
            $prop_config = unserialize($prop->config);
            $validation[$prop_name] = [];
            if ($prop->required=='1'){
                if ($prop->many=='1'){
                    $validation[$prop_name.'_1'][] = 'required';
                }   else    {
                    $validation[$prop_name][] = 'required';
                }
            }
            $field = new IBLockFieldController::$fields[$prop->type];
            $fieldValidations = $field->getFormValidation($prop_config);
            $validation[$prop_name] = implode('|', $fieldValidations);
        }
        //Проверяем валидацию
        $v = Validator::make($this->attributes, $validation);
        if ($v->fails()){
            return $v->errors();
        }
        //Удаляем свойства из объекта, что бы не было ошибки
        $props_values = [];
        foreach($props as $prop) {
            $prop_name = 'prop_' . $prop->alias;
            $props_values['prop_' . $prop->alias] = [];
            if ($prop->many=='0'){
                $props_values['prop_' . $prop->alias][] = $this->$prop_name;
                unset($this->$prop_name);
            }   else    {
                $i = 1;
                while(true){
                    $prop_name = 'prop_'.$prop->alias.'_'.$i;
                    if (!isset($this->$prop_name)) break;
                    $props_values['prop_'.$prop->alias][] = $this->$prop_name;
                    unset($this->$prop_name);
                    $i++;
                }
            }
        }
        //Начинаем транзакцию
        DB::beginTransaction();

        if ($this->id) IBLockSectionPropValue::where('section_id',$this->id)->delete();

        //Сохраняем раздел
        $s = parent::save($options);

        //Сохраняем свойства, если раздел сохранен успешно
        if ($s===true){
            foreach($props as $prop){
                foreach($props_values['prop_'.$prop->alias] as $prop_value){
                    $model = new IBlockSectionPropValue;
                    $model->prop_id = $prop->id;
                    $model->section_id = $this->id;
                    $model->value = $prop_value;
                    if (!$model->save()){
                        DB::rollback();
                        return false;
                    }
                }
            }
            DB::commit();
            return true;
        }   else    {
            DB::rollback();
            return $s;
        }
    }
}
