<?php

namespace App\Providers;

use App\Http\Controllers\Core\CacheController;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    private static $user_groups = false;

    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        $gates = null;//CacheController::get('gates');
        if ($gates===null){
            $gates = Gate::with('groups')->get();
            CacheController::put('gates',serialize($gates),['rights']);
        }
        foreach($gates as $g){
            $gate->define($g->alias, function($user) use($g){
                if (self::$user_groups===false){
                    self::$user_groups = $user->groups;
                }
                foreach(self::$user_groups as $group){
                    foreach($g->groups as $ggroup) {
                        if($group->id==$ggroup->id) return true;
                    }
                }
                return false;
            });
        }
    }
}
