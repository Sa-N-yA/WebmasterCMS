<?php

namespace App\Providers;

use App\IBlockSectionProp;
use Blade;
use App\Events\IBlockChanged;
use Event;
use App\GroupOlympic;
use App\Olympic;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use App\IBlockItem;
use App\IBlockSection;
use DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('component', function($params) {
            return "<?php echo \\App\\Http\\Controllers\\Components\\ComponentController::IncludeComponent($params);?>";
        });
        IBlockItem::saved(function($item){
            Event::fire(new IBlockChanged($item));
        });
        IBlockSection::saved(function($item){
            Event::fire(new IBlockChanged($item));
        });
        IBlockItem::deleting(function($item){
            Event::fire(new IBlockChanged($item));
        });
        IBlockSection::deleting(function($item){
            Event::fire(new IBlockChanged($item));
        });
        DB::listen(function($sql){
        });

        DB::update('UPDATE iblock_items SET deleted_at = NULL WHERE active_from IS NOT NULL AND active_to IS NOT NULL AND active_from <= NOW() AND active_to >= NOW()');
        DB::update('UPDATE iblock_items SET deleted_at = NOW() WHERE active_from IS NOT NULL AND active_to IS NOT NULL AND NOT (active_from <= NOW() AND active_to >= NOW())');
        DB::update('UPDATE iblock_sections SET deleted_at = NULL WHERE active_from IS NOT NULL AND active_to IS NOT NULL AND active_from <= NOW() AND active_to >= NOW()');
        DB::update('UPDATE iblock_sections SET deleted_at = NOW() WHERE active_from IS NOT NULL AND active_to IS NOT NULL AND NOT (active_from <= NOW() AND active_to >= NOW())');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
