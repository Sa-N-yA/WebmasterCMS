<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\IBLockItemPropValue;
use Config;
use Validator;
use App\Http\Controllers\Core\Modules\IBlockFields\IBLockFieldController;

class IBlockItemProp extends Model
{
    protected $table = 'iblock_item_props';
    public $timestamps = false;

    public function iblock(){
        return $this->belongsTo('\\App\\IBlock', 'iblock_id');
    }

    public function values(){
        return $this->hasMany('\\App\\IBlockItemPropValue', 'prop_id');
    }

    public function save(array $options = Array()){
        $type = $this->type;
        $validations = [
            'name' => 'required|min:3|max:255',
            'iblock_id'=>'exists:iblock,id',
            'required'=>'required|in:0,1',
            'many'=>'required|in:0,1',
            'type' => 'required|in:'.implode(',',array_keys(IBlockFieldController::$fields)),
            'alias' => 'required|min:3|max:255|unique:iblock_item_props,alias'.(($this->id?','.$this->id:',null')).',id,iblock_id,'.$this->iblock_id.'|regex:([a-zA-Z][a-zA-Z0-9_]*)'
        ];

        if (isset(IBlockFieldController::$fields[$type])){
            $field = new IBLockFieldController::$fields[$type];
            $fieldValidations = $field->getCreateValidation();
            foreach($fieldValidations as $k=>$v) $validations[$k] = $v;
            $configs = $field->getConfig();
            $arConfig = [];
            foreach($configs as $config){
                $arConfig[$config] = $this->$config;
            }
            $this->config = serialize($arConfig);
        }


        $v = Validator::make($this->attributes, $validations);

        foreach(IBLockFieldController::$fields as $field){
            $field = new $field;
            foreach($field->getConfig() as $t){
                unset($this->$t);
            }
        }
        if ($v->fails()) return $v->errors();
        return parent::save();
    }
}