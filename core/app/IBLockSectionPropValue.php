<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IBLockSectionPropValue extends Model
{
    protected $table = "iblock_section_props_values";
    public $timestamps = false;

    public function prop(){
        return $this->hasOne("\\App\\IBlockSectionProp", 'prop_id');
    }

    public function section(){
        return $this->belongsTo("\\App\\IBlockSections", 'item_id');
    }

}
