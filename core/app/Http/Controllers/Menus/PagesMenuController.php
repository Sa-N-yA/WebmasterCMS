<?php

namespace App\Http\Controllers\Menus;

use App\Http\Controllers\Core\SiteController;

class PagesMenuController extends MenuController
{
    private $maxDepth = 0, $ignore = [];
    public function run($params){
        if (!isset($params['depth'])){
            $this->maxDepth = 0;
        }   else    {
            $this->maxDepth = $params['depth'];
        }
        if (!isset($params['parent_id'])){
            $parent_id = 0;
        }   else    {
            $parent_id = $params['parent_id'];
        }
        if (!isset($params['ignore'])){
            $this->ignore = [];
        }   else    {
            $this->ignore = $params['ignore'];
        }

        $pages = SiteController::getPages();
        $result = [];
        foreach($pages as $page){
            if ($page->parent_id == $parent_id){
                $res = $this->handlePage($page, $pages, '', 0);
                if ($res!=null){
                    $result[] = $res;
                }
            }
        }
        return $result;
    }

    private function handlePage($page, $pages, $parentLink, $depth){
        if ($depth>$this->maxDepth) return [];
        $children = [];
        if ($depth==0 && $page->alias=='general') $page->alias = '';
        foreach($pages as $p){
            if ($p->parent_id == $page->id){
                $res = $this->handlePage($p,$pages, $parentLink.'/'.$page->alias, $depth+1);
                if ($res!=null) $children[] = $res;
            }
        }
        $result = [
            'link'=>$parentLink.'/'.$page->alias,
            'name'=>$page->name,
            'children'=>$children
        ];
        if (in_array($parentLink.'/'.$page->alias,$this->ignore)) return null;
        return $result;
    }
}
