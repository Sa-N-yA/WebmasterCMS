<?php

namespace App\Http\Controllers\Menus;

use App\Http\Controllers\Controller;

abstract class MenuController extends Controller
{
    abstract public function run($params);
}
