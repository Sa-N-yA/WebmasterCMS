<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 28.12.2016
 * Time: 22:44
 */

namespace App\Http\Controllers\Core\Admin;


use App\Http\Controllers\Controller;
use Session;
use Config;

class WebFormsController extends Controller
{
    public function exec($arPage){
        return view("core.admin.webforms.list");
    }
}