<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 28.12.2016
 * Time: 22:44
 */

namespace App\Http\Controllers\Core\Admin;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\AuthController;
use App\Http\Controllers\Core\Modules\ModuleController;
use Session;
use Gate;
use Route;
use Auth;
use Debug;
use Config;

class MainController extends Controller
{
    public static function getRoutes(){
        return [];
    }

    public function exec($page){
        $routes = $this->getRoutes();
        foreach ($routes as $route=>$view){
            if ($this->checkPage($route, $page, $result)){
                return $this->$view($result);
            }
        }
        return "";
    }

    public function main($page){
        if (!Gate::check('show_debug_panel')){
            Debug::disable();
        }
        if (Auth::user()===null){
            return $this->auth();
        }   else    {
            if (!Gate::check('access_to_admin_panel')){
                abort(403);
            }
            if ($page=='logout'){
                AuthController::logout();
                return redirect('/admin/main');
            }
            $arPage = explode('/',$page);
            $dataModules = ModuleController::build($arPage);

            view()->share('top_menu', $dataModules['top_menu']);

            if ($dataModules['current_module']=="") {
                return view('core.admin.index');
            }   else    {
                $moduleName = "\\App\\Http\\Controllers\\Core\\Admin\\".$dataModules['current_module']."Controller";
                $module = new $moduleName;
                return $module->exec(implode('/',array_slice($arPage,1)));
            }
        }
    }

    public function auth(){
        return view('core.admin.auth');
    }

    public function checkPage($pattern, $page, &$result){
        $pattern = str_replace('{$id}', '([0-9]+)',$pattern);
        $pattern = str_replace('{$alias}', '([a-zA-Z_0-9]+)',$pattern);
        $pattern = str_replace('/', '\/',$pattern);
        if (preg_match('/^'.$pattern.'$/', $page, $mathes)){
            $result = $mathes;
            return true;
        }   else    {
            return false;
        }
    }
}