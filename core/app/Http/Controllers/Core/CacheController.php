<?php
namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
use Cache;
use Log;
class CacheController extends Controller{
    private static $prefixTag = "_tag_";
    private static $duration = 10;

    public static function useCache(){
        return true;
    }

    public static function put($key, $value, $tags){
        Cache::put($key, $value, self::$duration);
        foreach($tags as $tag) self::putTag($key, $tag);
    }

    public static function putTag($key, $tag){
        $cache = Cache::get(self::$prefixTag.$tag);
        if ($cache==null) $cache = $key;
        else if (!in_array($key, explode("\n",$cache))) $cache.=$key."\n";
        Cache::put(self::$prefixTag.$tag, $cache, self::$duration);
        $tags = Cache::get(self::$prefixTag.'tags');
        if ($tags!=null) {
            $tags = unserialize($tags);
            if (!in_array($tag, $tags)) $tags[] = $tag;
        }   else    {
            $tags = [$tag];
        }
        Cache::put(self::$prefixTag.'tags',serialize($tags),self::$duration);
    }

    public static function getByTag($tag){
        $cacheTags = Cache::get(self::$prefixTag.$tag);
        if ($cacheTags!=null){
            return explode("\n", $cacheTags);
        }
        return [];
    }

    public static function deleteByTag($tag){
        $cacheTags = self::getByTag($tag);
        foreach ($cacheTags as $cacheKey) Cache::forget($cacheKey);
        Cache::forget(self::$prefixTag.$tag);
    }

    public static function get($key){
        if (!self::useCache()){
            return null;
        }
        return Cache::get($key);
    }
}