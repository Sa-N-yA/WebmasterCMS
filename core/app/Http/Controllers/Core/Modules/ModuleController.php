<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 28.12.2016
 * Time: 22:44
 */

namespace App\Http\Controllers\Core\Modules;


use App\Http\Controllers\Controller;
use Session;
use Config;
use Gate;
abstract class ModuleController extends Controller
{
    public abstract function BuildTopMenu();

    public static function build($arPage){
        $modules = Config::get('cms.modules');
        $topMenu = [];
        $current_module = "";
        foreach($modules as $moduleKey=>$module){
            if (!$module['enabled']) continue;
            if (!Gate::check($module['alias'].'_module')) continue;
            $moduleName = "\\App\\Http\\Controllers\\Core\\Modules\\".$moduleKey."ModuleController";
            $moduleClass = new $moduleName;
            $topMenu[] = [
                'text'=>$module['name'],
                'href'=>'/admin/'.$module['alias'],
                'children'=>$moduleClass->BuildTopMenu()
            ];
            if (isset($arPage[0]) && $module['alias']==$arPage[0]){
                $current_module = $moduleKey;
            }
        }
        return ['top_menu'=>$topMenu, 'current_module'=>$current_module];
    }
}