<?php

namespace App\Http\Controllers\Core\Modules;

use App\Http\Controllers\Components\IBlockData\ItemsController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CacheController;
use App\IBlock;
use App\IBlockSection;
use DB;

class IBlockModuleController extends ModuleController{

    public function BuildTopMenu()
    {
        $iblocks = IBlock::all();
        $children = [
            [
                'text'=>'Список',
                'href'=>'/admin/iblock/list'
            ],
            [
                'text'=>'Создать новый инфоблок',
                'href'=>'/admin/iblock/create'
            ],[
                'text'=>'-'
            ]
        ];
        foreach($iblocks as $iblock){
            $children[] = [
                'text'=>$iblock->name,
                'href'=>'/admin/iblock/'.$iblock->id
            ];
        }
        return $children;
    }

    private static function _GetWhere($filter){
        $ops = ['=','!=','<>','>=','<=','>','<','between', 'not between', 'in', 'not in', 'like', 'is'];
        $key = $filter[0];
        $op = count($filter)==2?'=':$filter[1];
        $val = count($filter)==2?$filter[1]:$filter[2];
        if (!in_array($op,$ops)) return "";
        $key = addslashes($key);
        if (is_numeric($val)){
            return "$key $op ".addslashes($val);
        }   else if (is_array($val) && ($op=='in' || $op=='not in'))    {
            if (count($val)==0){
                return "1=0";
            }   else    {
                return "$key $op (".addslashes(implode(',',$val)).")";
            }
        }   else if ($op=='is' && ($val=='null' || $val=='not null'))    {
            return "$key $op $val";
        }
        return "$key $op '".addslashes($val)."'";
    }

    public static function GetIBlocks($fields = []){
        $iblocks = IBlock::all();
        $res = [];
        foreach ($iblocks as $iblock){
            $res[$iblock->id] = [
                'id'=>$iblock->id,
                'alias'=>$iblock->alias,
                'name'=>$iblock->name
            ];
            if (in_array('sections.count',$fields)){
                $res[$iblock->id]['sections.count'] = self::GetCountSections($iblock->id, ['id','>','0']);
            }
            if (in_array('items.count',$fields)){
                $res[$iblock->id]['items.count'] = 15;
            }
        }
        return $res;
    }

    private static function getCompProp($propName){
        $prop = explode('.',$propName);
        if (count($prop)==2){
            return [true,$prop[1]];
        }   else    {
            return [false, $propName];
        }
    }

    private static function _GetWhereProp($f)
    {
        list($prop, $key) = self::getCompProp($f[0]);
        $c1 = self::_GetWhere(['props.alias',$key]);
        $f[0] = 'vals.value';
        $c2 = self::_GetWhere($f);
        return "($c1 AND $c2)";
    }

    public static function GetSections($iblock_id, $fields = [], $filters = [], $paginate = [], $order = 'id'){
        $iblock_id = addslashes($iblock_id);

        $cacheKey = serialize([
            'method'=>'iblock_sections',
            'iblock_id'=>$iblock_id,
            'fields'=>$fields,
            'filters'=>$filters,
            'paginate'=>$paginate,
            'order'=>$order
        ]);
        $cache = CacheController::get($cacheKey);
        if ($cache){
            return unserialize($cache);
        }   else    {
            $offer = 0;
            $limit = 0;
            if ($paginate != [] && isset($paginate['page'],$paginate['count']) && $paginate['page']>0 && $paginate['count']>0){
                $offer = ($paginate['page']-1)*$paginate['count'];
                $limit = $paginate['count'];
            }

            $result = [];

            $filtersProps = [];
            $filtersNotProps = [];
            $isActive = false;
            foreach($filters as $filter){
                list($isProp, $key) = self::getCompProp($filter[0]);
                if ($isProp){
                    $filtersProps[] = $filter;
                }   else    {
                    if ($key=='active'){
                        $isActive = true;
                        if ($filter[1]=='y' || $filter[1]=='Y'){
                            $filtersNotProps[] = ['deleted_at','is','null'];
                        }   else if($filter[1]=='n' || $filter[1]=='N') {
                            $filtersNotProps[] = ['deleted_at','is','not null'];
                        }   else if($filter[1]=='all' || $filter[1]=='ALL'){
                        }
                    }
                    $filtersNotProps[] = $filter;
                }
            }
            if (!$isActive){
                $filtersNotProps[] = ['deleted_at','is','null'];
            }
            $fieldsProps = [];
            $fieldsNotProps = [];
            foreach($fields as $field){
                list($isProp, $key) = self::getCompProp($field);
                if ($isProp){
                    $fieldsProps[] = "'".$key."'";
                }   else    {
                    $fieldsNotProps[] = $key;
                }
            }
            $queryNotIn = "";
            if ($filters != []){
                $queryNotIn = "SELECT
                  sections.id
                FROM
                  iblock_sections AS sections
                ";
                if ($filtersProps!=[]){
                    $queryNotIn.="
                        JOIN
                            iblock_section_props AS props ON props.iblock_id = $iblock_id
                        JOIN
                            iblock_section_props_values AS vals ON props.id = props.id AND sections.id = vals.section_id
                    ";
                }
                $queryNotInWhere = [];
                foreach($filtersProps as $f){
                    $queryNotInWhere[] = self::_GetWhereProp($f);
                }
                if ($queryNotInWhere!=[]) {
                    $queryNotInWhere = [
                        implode(' OR ', $queryNotInWhere)
                    ];
                }
                foreach($filtersNotProps as $f){
                    if ($f[0]=='active') continue;
                    $f[0] = 'sections.'.$f[0];
                    $queryNotInWhere[] = self::_GetWhere($f);
                }
                array_filter($queryNotInWhere);
                $queryNotInWhere = implode(' AND ', $queryNotInWhere);
                if ($queryNotInWhere!="") $queryNotIn.=" WHERE NOT(".$queryNotInWhere.")";
                else $queryNotIn="";
            }
            $select = ['sections.id','sections.deleted_at'];
            foreach ($fieldsNotProps as $f){
                if ($f=='active') continue;
                $select[] = 'sections.'.$f;
            }
            if ($fieldsProps!=[]){
                $select[] = 'props.alias AS palias';
                $select[] = 'props.many AS pmany';
                $select[] = 'vals.value AS pvalue';
            }
            $select = implode(',',$select);
            $query = "
                SELECT
                  $select
                FROM
                  iblock_sections AS sections
            ";
            if ($fieldsProps!=[]){
                $query.="
                    JOIN
                        iblock_section_props AS props ON props.iblock_id = $iblock_id
                    LEFT JOIN
                        iblock_section_props_values AS vals ON props.id = vals.prop_id AND sections.id = vals.section_id
                ";
            }
            $query.=" WHERE sections.iblock_id = $iblock_id";
            if ($queryNotIn!=""){
                $query.=" AND sections.id NOT IN ($queryNotIn)";
            }
            if ($fieldsProps!=[]){
                $query.=" AND props.alias IN (".implode(',',$fieldsProps).")";
            }
            if (!empty($order)){
                $query.= " ORDER BY $order";
            }
            if ($limit>0 || $offer>0){
                if ($offer==0){
                    $query.=" LIMIT $limit";
                }   else {
                    $query.=" LIMIT $offer,$limit";
                }
            }
            $dbResult = DB::select($query);
            $resultIDs = [];
            $result = [];
            foreach($dbResult as $r){
                $r = (array)$r;
                $item_id = $r['id'];
                if (!in_array($item_id, $resultIDs)){
                    $resultIDs[] = $item_id;
                    $result[$item_id] = [
                        'id' => $item_id
                    ];
                }
                foreach($r as $key=>$value){
                    if ($key=='palias'){
                        if ($r['pvalue']==null){
                            if ($r['pmany']=='1'){
                                $result[$item_id][$r['palias']] = [];
                            }   else    {
                                $result[$item_id][$r['palias']] = '';
                            }
                        }   else {
                            if (isset($result[$item_id][$r['palias']]) && !is_array($result[$item_id][$r['palias']])) {
                                $result[$item_id][$r['palias']] = [$result[$item_id][$r['palias']]];
                            }
                            if (isset($result[$item_id][$r['palias']]) && is_array($result[$item_id][$r['palias']])) {
                                $result[$item_id][$r['palias']][] = $r['pvalue'];
                            } else {
                                if ($r['pmany'] == '1') {
                                    $result[$item_id][$r['palias']] = [$r['pvalue']];
                                } else {
                                    $result[$item_id][$r['palias']] = $r['pvalue'];
                                }
                            }
                        }
                    }   else if ($key=='pvalue'){
                    }   else if ($key=='pmany'){
                    }   else    {
                        if ($key == 'deleted_at'){
                            if ($value==null){
                                $result[$item_id]['active'] = 'Y';
                            }   else    {
                                $result[$item_id]['active'] = 'N';
                            }
                        }   else {
                            $result[$item_id][$key] = $value;
                        }
                    }
                }
            }

            $result = ['items' => $result];
            if ($paginate!=[]){
                $query = "
                  SELECT
                    COUNT(id) AS count
                  FROM
                    iblock_sections AS sections
                  WHERE
                    sections.iblock_id AS $iblock_id
                ";
                if ($queryNotIn!="") {
                    $query.=" AND
                    sections.id NOT IN ($queryNotIn)";
                }
                $res = DB::select($query);
                $result['count'] = $res[0]->count;
            }
            CacheController::put($cacheKey, serialize($result),['iblock_'.$iblock_id]);
            return $result;
        }
    }

    public static function GetItems($iblock_id, $fields = [], $filters = [], $paginate = [], $order = 'id'){
        $iblock_id = addslashes($iblock_id);

        $cacheKey = serialize([
            'method'=>'iblock_items',
            'iblock_id'=>$iblock_id,
            'fields'=>$fields,
            'filters'=>$filters,
            'paginate'=>$paginate,
            'order'=>$order
        ]);

        $cache = CacheController::get($cacheKey);
        if ($cache){
            return unserialize($cache);
        }   else    {
            $offer = 0;
            $limit = 0;
            if ($paginate != [] && isset($paginate['page'],$paginate['count']) && $paginate['page']>0 && $paginate['count']>0){
                $offer = ($paginate['page']-1)*$paginate['count'];
                $limit = $paginate['count'];
            }


            $result = [];

            $filtersProps = [];
            $filtersNotProps = [];
            $isActive = false;
            foreach($filters as $filter){
                list($isProp, $key) = self::getCompProp($filter[0]);
                if ($isProp){
                    $filtersProps[] = $filter;
                }   else    {
                    if ($key=='active'){
                        $isActive = true;
                        if ($filter[1]=='y' || $filter[1]=='Y'){
                            $filtersNotProps[] = ['deleted_at','is','null'];
                        }   else if($filter[1]=='n' || $filter[1]=='N') {
                            $filtersNotProps[] = ['deleted_at','is','not null'];
                        }   else if($filter[1]=='all' || $filter[1]=='ALL'){
                        }
                    }
                    $filtersNotProps[] = $filter;
                }
            }
            if (!$isActive){
                $filtersNotProps[] = ['deleted_at','is','null'];
            }
            $fieldsProps = [];
            $fieldsNotProps = [];
            foreach($fields as $field){
                list($isProp, $key) = self::getCompProp($field);
                if ($isProp){
                    $fieldsProps[] = "'".$key."'";
                }   else    {
                    $fieldsNotProps[] = $key;
                }
            }
            $queryNotIn = "";
            if ($filters != []){
                $queryNotIn = "SELECT
                  items.id
                FROM
                  iblock_items AS items
                ";
                if ($filtersProps!=[]){
                    $queryNotIn.="
                        JOIN
                            iblock_item_props AS props ON props.iblock_id = $iblock_id
                        JOIN
                            iblock_item_props_values AS vals ON props.id = props.id AND items.id = vals.item_id
                    ";
                }
                $queryNotInWhere = [];
                foreach($filtersProps as $f){
                    $f = self::_GetWhereProp($f);
                    if ($f) $queryNotInWhere[] = $f;
                }
                if ($queryNotInWhere!=[]) {
                    $queryNotInWhere = [
                        implode(' OR ', $queryNotInWhere)
                    ];
                }
                foreach($filtersNotProps as $f){
                    if ($f[0]=='active') continue;
                    $f[0] = 'items.'.$f[0];
                    $queryNotInWhere[] = self::_GetWhere($f);
                }
                $queryNotInWhere = array_filter($queryNotInWhere);
                $queryNotInWhere = implode(' AND ', $queryNotInWhere);
                if ($queryNotInWhere!="") $queryNotIn.=" WHERE NOT(".$queryNotInWhere.")";
                else $queryNotIn="";
            }
            $select = ['items.id','items.deleted_at'];
            foreach ($fieldsNotProps as $f) $select[] = 'items.'.$f;

            if ($fieldsProps!=[]){
                $select[] = 'props.alias AS palias';
                $select[] = 'props.many AS pmany';
                $select[] = 'vals.value AS pvalue';
            }

            $select = implode(',',$select);
            $query = "
                SELECT
                  $select
                FROM
                  iblock_items AS items
            ";

            if ($fieldsProps!=[]){
                $query.="
                    JOIN
                        iblock_item_props AS props ON props.iblock_id = $iblock_id
                    LEFT JOIN
                        iblock_item_props_values AS vals ON props.id = vals.prop_id AND items.id = vals.item_id
                ";
            }
            $query.=" WHERE items.iblock_id = $iblock_id";
            if ($queryNotIn!=""){
                $query.=" AND items.id NOT IN ($queryNotIn)";
            }
            if ($fieldsProps!=[]){
                $query.=" AND props.alias IN (".implode(',',$fieldsProps).")";
            }
            if (!empty($order)){
                $query.= " ORDER BY $order";
            }
            if ($limit>0 || $offer>0){
                if ($offer==0){
                    $query.=" LIMIT $limit";
                }   else {
                    $query.=" LIMIT $offer,$limit";
                }
            }
            $dbResult = DB::select($query);
            $resultIDs = [];
            $result = [];
            foreach($dbResult as $r){
                $r = (array)$r;
                $item_id = $r['id'];
                if (!in_array($item_id, $resultIDs)){
                    $resultIDs[] = $item_id;
                    $result[$item_id] = [
                        'id' => $item_id
                    ];
                }
                foreach($r as $key=>$value){
                    if ($key=='palias'){
                        if ($r['pvalue']==null){
                            if ($r['pmany']=='1'){
                                $result[$item_id][$r['palias']] = [];
                            }   else    {
                                $result[$item_id][$r['palias']] = '';
                            }
                        }   else {
                            if (isset($result[$item_id][$r['palias']]) && !is_array($result[$item_id][$r['palias']])) {
                                $result[$item_id][$r['palias']] = [$result[$item_id][$r['palias']]];
                            }
                            if (isset($result[$item_id][$r['palias']]) && is_array($result[$item_id][$r['palias']])) {
                                $result[$item_id][$r['palias']][] = $r['pvalue'];
                            } else {
                                if ($r['pmany'] == '1') {
                                    $result[$item_id][$r['palias']] = [$r['pvalue']];
                                } else {
                                    $result[$item_id][$r['palias']] = $r['pvalue'];
                                }
                            }
                        }
                    }   else if ($key=='pvalue'){
                    }   else if ($key=='pmany'){
                    }   else    {
                        if ($key == 'deleted_at'){
                            if ($value==null){
                                $result[$item_id]['active'] = 'Y';
                            }   else    {
                                $result[$item_id]['active'] = 'N';
                            }
                        }   else {
                            $result[$item_id][$key] = $value;
                        }
                    }
                }
            }
            $result = ['items' => $result];
            if ($paginate!=[]){
                $query = "
                  SELECT
                    COUNT(id) AS count
                  FROM
                    iblock_items AS items
                  WHERE
                    items.iblock_id = $iblock_id
                ";
                if ($queryNotIn!="") {
                    $query.=" AND
                    items.id NOT IN ($queryNotIn)";
                }
                $res = DB::select($query);
                $result['count'] = $res[0]->count;
            }
            CacheController::put($cacheKey, serialize($result),['iblock_'.$iblock_id]);
            return $result;
        }
    }

    public static function GetItemByID($iblock_id, $item_id, $fields = [], $active = 'Y'){
        $res = self::GetItems($iblock_id, $fields, [['id',$item_id],['active',$active]]);
        if (isset($res['items'][$item_id])){
            return $res['items'][$item_id];
        }
        return false;
    }

    public static function GetItemByAlias($iblock_id, $item_alias, $fields, $active = 'Y'){
        $res = self::GetItems($iblock_id,$fields, [['alias'=>$item_alias],['active',$active]]);
        if (isset($res['items'][0])){
            return $res['items'][0];
        }
        return false;
    }

    public static function GetSectionByID($iblock_id, $section_id, $fields = [], $active = 'Y'){
        $res = self::GetSections($iblock_id,$fields, [['id',$section_id],['active',$active]]);
        if (isset($res['items'][$section_id])){
            return $res['items'][$section_id];
        }
        return false;
    }

    public static function GetSectionByAlias($iblock_id, $section_alias, $fields, $active = 'Y'){
        $res = self::GetSections($iblock_id,$fields, [['alias'=>$section_alias],['active',$active]]);
        if (isset($res['items'][0])){
            return $res['items'][0];
        }
        return false;
    }

    public static function GetPathSection($iblock_id, $section_id, $fields = []){
        if ($section_id==0) return false;
        if (!in_array('parent_id', $fields)) $fields[] = 'parent_id';

        $cacheKey = serialize([
            'method'=>'GetPathSection',
            'params'=>[
                'section_id' => $section_id,
                'fields' => $fields
            ]
        ]);
        $cache = CacheController::get($cacheKey);

        if ($cache){
            return unserialize($cache);
        }   else    {
            $path = [];
            $section = self::GetSectionByID($iblock_id,$section_id, $fields);
            $parent = self::GetPathSection($iblock_id, $section_id, $fields);
            if ($parent!==false){
                $path = $parent;
            }
            $path[] = $section;
            return $path;
        }
    }
}