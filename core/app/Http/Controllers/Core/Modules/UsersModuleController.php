<?php

namespace App\Http\Controllers\Core\Modules;

use App\Http\Controllers\Controller;

class UsersModuleController extends ModuleController{
    public function BuildTopMenu()
    {
        return [
            [
                'text'=>'Список',
                'href'=>'/admin/users/list'
            ],
            [
                'text'=>'Группы',
                'href'=>'/admin/users/groups/list'
            ],
            [
                'text'=>'Гейты',
                'href'=>'/admin/users/gates/list'
            ]
        ];
    }
}