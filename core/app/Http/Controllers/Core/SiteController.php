<?php
namespace App\Http\Controllers\Core;
use Debug;
use App\Http\Controllers\Controller;
use App\Page;
use App\Template;
use Cache;
use Route;
use Gate;

class SiteController extends Controller{
    private static $uriPrefix = 'uri_';
    private static $breadCrumbs = [];
    private static $template = "";
    private static $uri = [];

    public static function getTemplate(){
        return self::$template;
    }

    public static function getPages(){
        $cache = CacheController::get('pages');
        if ($cache){
            return unserialize($cache);
        }   else    {
            $pages = Page::all();
            CacheController::put('pages',serialize($pages),['uri']);
            return $pages;
        }
    }

    public static function run($u){
        $cache = false;

        if (!Gate::check('show_debug_panel')){
            Debug::disable();
        }
        self::$uri = explode('/',$u);
        if ($u=='/') self::$uri = [];

        $uri = CacheController::get(self::$uriPrefix.base64_encode($u));
        if ($uri) {
            $uri = unserialize($uri);
            $view = $uri['view'];
            $template = $uri['template'];
            self::$breadCrumbs = $uri['breadCrumbs'];
            $cache = true;
        }   else {
            if ($u == '/') {
                $page = Page::where('alias', 'general')->firstOrFail();
                $template = Template::where('id', $page->template)->firstOrFail()->alias;
                $view = $page->view;
            } else {
                $pages = self::getPages();
                $arUri = self::$uri;
                $parent = 0;
                $view = null;
                $path = '';
                foreach($arUri as $a){
                    $found = false;
                    foreach($pages as $page){
                        if ($page->alias==$a && $page->parent_id==$parent){
                            $view = $page->view;
                            $template = $page->template;
                            $parent = $page->id;
                            $path.= '/'.$page->alias;
                            self::pushBreadCrumb($page->name,$path);
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) break;
                    $template = Template::where('id',$template)->firstOrFail()->alias;
                }
            }
        }

        if ($view==null) abort(404);
        if (!view()->exists('templates.'.$template.'.main')) abort(404);
        if (!view()->exists('site.'.$view)) abort(404);
        if (!$cache){
            CacheController::put(self::$uriPrefix.base64_encode($u), serialize(['view'=>$view, 'template'=>$template, 'breadCrumbs'=>self::$breadCrumbs]),['uri']);
        }
        self::$template = $template;
        return view('core/site',['template'=>$template, 'view'=>$view]);
    }

    public static function pushBreadCrumb($title, $link = ""){
        self::$breadCrumbs[] = ['title'=>$title, 'link'=>$link];
    }

    public static function getBreadCrumbs(){
        $breadcrumbs = self::$breadCrumbs;
        $breadcrumbs[count($breadcrumbs)-1]['link'] = "";
        return self::$breadCrumbs;
    }

    public static function getUri(){
        return self::$uri;
    }
}