<?php

namespace App\Http\Controllers\Components\Core;
use App\Http\Controllers\Components\ComponentController;

class MenuController extends ComponentController
{
    public static function run($params){
        $result = [];
        if (isset($params['type'])){
            ini_set('display_errors','on');
            error_reporting(E_ALL);
            $menuContoller = "App\\Http\\Controllers\\Menus\\".$params['type']."MenuController";
            $menu = new $menuContoller;
            $result['items'] = $menu->run($params);
        }
        return $result;
    }
}