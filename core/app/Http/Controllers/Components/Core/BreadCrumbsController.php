<?php

namespace App\Http\Controllers\Components\Core;
use App\Http\Controllers\Components\ComponentController;
use App\Http\Controllers\Core\SiteController;

class BreadCrumbsController extends ComponentController
{
    public static function run(){
        return SiteController::getBreadCrumbs();
    }
}