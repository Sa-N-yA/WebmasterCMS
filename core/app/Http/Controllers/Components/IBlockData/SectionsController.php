<?php

namespace App\Http\Controllers\Components\IBlockData;

use App\Http\Controllers\Components\ComponentController;
use App\Http\Controllers\Core\SiteController;
use App\IBlockItem;
use App\IBlockSection;
use Route;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use IBlockModule;

class SectionsController extends ComponentController
{
    public function run($params){
        $res = [];
        if ($params['iblock_id']){
            if (!isset($params['fields'])) $params['fields'] = [];
            if (!isset($params['filter'])) $params['filter'] = [];
            if (!isset($params['paginate'])) $params['paginate'] = [];
            if (!isset($params['order'])) $params['order'] = "id";
            $res = IBlockModule::GetSections($params['iblock_id'],$params['fields'],$params['filter'],$params['paginate'],$params['order']);
        }
        return $res;
    }
}
