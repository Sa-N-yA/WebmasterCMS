<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 02.01.2017
 * Time: 12:49
 */

namespace App\Http\Controllers\Ajax;
use Request;
use Storage;
use FilesModule;

class FilesAjaxController extends AjaxController
{
    public function get_items($opt){
        if (isset($opt['folder_id'])){
            $this->data = FilesModule::GetItems($opt['folder_id']);
            return $this->finish();
        }   else    {
            return $this->_404();
        }
    }

    public function create_folder($opt){
        if (isset($opt['name'], $opt['parent_id'])){
            $this->data = FilesModule::CreateFolder($opt['parent_id'],$opt['name']);
            if ($this->data instanceof \Illuminate\Support\MessageBag){
                $this->error = true;
            }
            return $this->finish();
        }
        return $this->_404();
    }

    public function load_files($opt){
        $this->data = FilesModule::CreateFile(Request::input('folder_id'), Request::file('file'));
        if ($this->data instanceof \Illuminate\Support\MessageBag){
            $this->error = true;
        }
        return $this->finish();
    }
}