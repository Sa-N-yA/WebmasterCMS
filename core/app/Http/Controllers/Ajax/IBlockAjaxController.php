<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers;
use App\Http\Requests;
use App\IBlock;
use App\IBlockItem;
use App\IBlockItemProp;
use App\IBLockItemPropValue;
use App\IBlockSection;
use App\IBlockSectionProp;
use IBlockModule;
use Config;
use \App\Http\Controllers\Core\Modules\IBlockFields\IBlockFieldController;

class IBlockAjaxController extends AjaxController
{
    public function create_iblock($opt){
        if (isset($opt['name'], $opt['alias'])){
            $iblock = new IBlock();
            $iblock->name = $opt['name'];
            $iblock->alias = $opt['alias'];
            $s = $iblock->save();
            if ($s===true){
                $this->res = 'Инфоблок успешно создан';
            }   else    {
                $this->data = $s;
                $this->error = true;
                return $this->finish();
            }
        }   else    {
            $this->res = "Неверный формат данных";
            $this->error = true;
        }
        return $this->finish();
    }

    public function update_iblock($opt){
        if (isset($opt['id'],$opt['name'], $opt['alias'])){
            $iblock = IBlock::find($opt['id']);
            if ($iblock==null) return $this->_404();
            $iblock->name = $opt['name'];
            $iblock->alias = $opt['alias'];
            $s = $iblock->save();
            if ($s===true){
                $this->res = 'Инфоблок успешно создан';
            }   else    {
                $this->data = $s;
                $this->error = true;
                return $this->finish();
            }
        }   else    {
            $this->res = "Неверный формат данных";
            $this->error = true;
        }
        return $this->finish();
    }

    public function update_item_prop($opt){
        $prop = IBlockItemProp::find($opt['id']);
        if ($prop==null) return $this->_404();
        $prop->name = $opt['name'];
        $prop->required = $opt['required'];
        $prop->many = $opt['many'];

        $fields = IBLockFieldController::$fields;
        foreach($fields as $field){
            $field = new $field;
            foreach($field->getConfig() as $t){
                $prop->$t = $opt[$t];
            }
        }
        $s = $prop->save();
        if ($s===true){
            $this->res = 'Свойство успешно создано';
        }   else    {
            $this->data = $s;
            $this->error = true;
        }
        return $this->finish();
    }
    public function create_item_prop($opt){
        $prop = new IBlockItemProp();
        $prop->iblock_id = $opt['iblock_id'];
        $prop->name = $opt['name'];
        $prop->alias = $opt['alias'];
        $prop->type = $opt['type'];
        $prop->required = $opt['required'];
        $prop->many = $opt['many'];

        $fields = IBLockFieldController::$fields;
        foreach($fields as $field){
            $field = new $field;
            foreach($field->getConfig() as $t){
                $prop->$t = $opt[$t];
            }
        }
        $s = $prop->save();
        if ($s===true){
            $this->res = 'Свойство успешно создано';
        }   else    {
            $this->data = $s;
            $this->error = true;
        }
        return $this->finish();
    }
    public function update_item($opt){
        $item = IBlockItem::withTrashed()->find($opt['id']);
        if (!$item) return $this->_404();
        foreach($opt as $k=>$v){
            if (strpos($k,'prop_')==0){
                $item->$k = $v;
            }   else    {
                if (in_array($k,['name','alias','parent_id', 'iblock_id', 'active_to', 'active_from'])){
                    $item->$k = $v;
                }   else    {
                    return $this->_404();
                }
            }
        }
        $s = $item->save();
        $this->error = $s!==true;
        $this->data = $s;
        return $this->finish();
    }
    public function create_item($opt){
        $item = new IBlockItem();
        foreach($opt as $k=>$v){
            if (strpos($k,'prop_')==0){
                $item->$k = $v;
            }   else    {
                if (in_array($k,['name','alias','parent_id', 'iblock_id'])){
                    $item->$k = $v;
                }   else    {
                    return $this->_404();
                }
            }
        }
        $s = $item->save();
        $this->error = $s!==true;
        $this->data = $s;
        return $this->finish();
    }
    public function update_section_prop($opt){
        $prop = IBlockSectionProp::find($opt['id']);
        if ($prop==null) return $this->_404();
        $prop->name = $opt['name'];
        $prop->required = $opt['required'];
        $prop->many = $opt['many'];

        $fields = IBLockFieldController::$fields;
        foreach($fields as $field){
            $field = new $field;
            foreach($field->getConfig() as $t){
                $prop->$t = $opt[$t];
            }
        }
        $s = $prop->save();
        if ($s===true){
            $this->res = 'Свойство успешно создано';
        }   else    {
            $this->data = $s;
            $this->error = true;
        }
        return $this->finish();
    }
    public function create_section_prop($opt){
        $prop = new IBlockSectionProp();
        $prop->iblock_id = $opt['iblock_id'];
        $prop->name = $opt['name'];
        $prop->alias = $opt['alias'];
        $prop->type = $opt['type'];
        $prop->required = $opt['required'];
        $prop->many = $opt['many'];

        $fields = IBLockFieldController::$fields;
        foreach($fields as $field){
            $field = new $field;
            foreach($field->getConfig() as $t){
                $prop->$t = $opt[$t];
            }
        }
        $s = $prop->save();
        if ($s===true){
            $this->res = 'Свойство успешно создано';
        }   else    {
            $this->data = $s;
            $this->error = true;
        }
        return $this->finish();
    }
    public function update_section($opt){
        $item = IBlockSection::withTrashed()->find($opt['id']);
        if (!$item) return $this->_404();
        foreach($opt as $k=>$v){
            if (strpos($k,'prop_')==0){
                $item->$k = $v;
            }   else    {
                if (in_array($k,['name','alias','parent_id', 'iblock_id', 'active_to', 'active_from'])){
                    $item->$k = $v;
                }   else    {
                    return $this->_404();
                }
            }
        }
        $s = $item->save();
        $this->error = $s!==true;
        $this->data = $s;
        return $this->finish();
    }
    public function create_section($opt){
        $item = new IBlockSection();
        foreach($opt as $k=>$v){
            if (strpos($k,'prop_')==0){
                $item->$k = $v;
            }   else    {
                if (in_array($k,['name','alias','parent_id', 'iblock_id'])){
                    $item->$k = $v;
                }   else    {
                    return $this->_404();
                }
            }
        }
        $s = $item->save();
        $this->error = $s!==true;
        $this->data = $s;
        return $this->finish();
    }

    public function get_sections($opt){
        if (!isset($opt['iblock_id'])) return $this->_404();
        if (!isset($opt['filter'])) $opt['filter'] = [];
        if (!isset($opt['fields'])) $opt['fields'] = [];
        if (!isset($opt['order'])) $opt['order'] = "id";
        $this->data = IBlockModule::GetSections($opt['iblock_id'], $opt['fields'],$opt['filter'],[],$opt['order']);
        return $this->finish();
    }
    public function get_items($opt){
        if (!isset($opt['iblock_id'])) return $this->_404();
        if (!isset($opt['filter']['sections_filter'])) $opt['filter']['sections_filter'] = [];
        if (!isset($opt['filter']['items_filter'])) $opt['filter']['items_filter'] = [];
        if (!isset($opt['fields'])) $opt['fields'] = [];
        if (!isset($opt['order'])) $opt['order'] = "id";
        $sections = IBlockModule::GetSections($opt['iblock_id'], $opt['fields'],$opt['filter']['sections_filter'],[],$opt['order'])['items'];
        $items = IBlockModule::GetItems($opt['iblock_id'], $opt['fields'],$opt['filter']['items_filter'],[],$opt['order'])['items'];
        $this->data = ['sections'=>$sections,'items'=>$items];
        return $this->finish();
    }

    public function remove_iblock($opt){
        if (isset($opt['id'])){
            $item = IBlock::find($opt['id']);
            if (!$item) return $this->_404();
            $item->delete();
            return $this->finish();
        }
        return $this->_404();
    }
    public function deactivate_section($opt){
        if (isset($opt['id'])){
            $section = IBlockSection::find($opt['id']);
            if (!$section) return $this->_404();
            $section->delete();
            return $this->finish();
        }
        return $this->_404();
    }
    public function restore_section($opt){
        if (isset($opt['id'])){
            $section = IBlockSection::onlyTrashed()->find($opt['id']);
            if (!$section) return $this->_404();
            IBlockSection::where('id',$opt['id'])->restore();
            return $this->finish();
        }
        return $this->_404();
    }
    public function remove_section($opt){
        if (isset($opt['id'])){
            $section = IBlockSection::onlyTrashed()->find($opt['id']);
            if (!$section) return $this->_404();
            $section->forceDelete();
            return $this->finish();
        }
        return $this->_404();
    }

    public function remove_section_prop($opt){
        if (isset($opt['id'])){
            $section = IBlockSectionProp::find($opt['id']);
            if (!$section) return $this->_404();
            $section->delete();
            return $this->finish();
        }
        return $this->_404();
    }

    public function remove_item_prop($opt){
        if (isset($opt['id'])){
            $section = IBlockItemProp::find($opt['id']);
            if (!$section) return $this->_404();
            $section->delete();
            return $this->finish();
        }
        return $this->_404();
    }

    public function deactivate_item($opt){
        if (isset($opt['id'])){
            $item = IBlockItem::find($opt['id']);
            if (!$item) return $this->_404();
            $item->delete();
            return $this->finish();
        }
        return $this->_404();
    }

    public function restore_item($opt){
        if (isset($opt['id'])){
            $item = IBlockItem::onlyTrashed()->find($opt['id']);
            if (!$item) return $this->_404();
            IBlockItem::where('id',$opt['id'])->restore();
            return $this->finish();
        }
        return $this->_404();
    }

    public function remove_item($opt){
        if (isset($opt['id'])){
            $item = IBlockItem::onlyTrashed()->find($opt['id']);
            if (!$item) return $this->_404();
            $item->forceDelete();
            return $this->finish();
        }
        return $this->_404();
    }

}
