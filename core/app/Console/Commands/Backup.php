<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\HttpKernel\Tests\Fragment\Bar;
use DB;

class Backup extends Command
{
    private $pathIgnore = [
        DIRECTORY_SEPARATOR.'core'.DIRECTORY_SEPARATOR.'storage',
    ];

    private $path;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:backup {--nodb}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make backup site';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->path = dirname(base_path());
        parent::__construct();
    }

    private function enterPassword(){
        $password = $this->secret('Enter the password archive');
        if (strlen($password)<6){
            $this->error('The password must be at least 6 characters!');
            return $this->enterPassword();
        }
        $confirmPassword = $this->secret('Confirm password archive');
        if ($password!=$confirmPassword){
            $this->error('The entered passwords do not match!');
            return $this->enterPassword();
        }
        return $password;
    }

    private function getFiles($path){
        if (in_array($path,$this->pathIgnore)) return [];
        $dirs = scandir($this->path.DIRECTORY_SEPARATOR.$path);
        $files = [];
        foreach($dirs as $dir){
            if ($dir=='.' || $dir=='..') continue;
            $ePath = $path.DIRECTORY_SEPARATOR.$dir;
            if (is_dir($this->path.$ePath)){
                $files = array_merge($files, $this->getFiles($ePath));
            }   else    {
                $files[] = $ePath;
            }
        }
        return $files;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $isBackupDB = !$this->option('nodb');
        //$password = $this->enterPassword();


        $dbFileName = $this->path.DIRECTORY_SEPARATOR."backup.sql";
        if ($isBackupDB){
            $this->line("Begin backup database");

            $execStr = "mysqldump";
            $dbUsername = DB::connection()->getConfig('username');
            $dbPassword = DB::connection()->getConfig('password');
            $dbName = DB::connection()->getConfig('database');
            if (!empty($dbUsername)){
                $execStr.=" -u ".$dbUsername;
            }
            if (!empty($dbPassword)){
                $execStr.=" -p ".$dbPassword;
            }
            $execStr.=" ".$dbName;

            exec($execStr. " > ".$dbFileName);
        }
        echo "Begin calculate the number of files\n";
        $files = $this->getFiles('');
        $countFiles = count($files);
        $this->line("Calculate the number of files equals ".$countFiles."\n");

        $archive = new \ZipArchive();
        $archiveName = '/core/storage/backups/backup'.date('Ymd_His').'.zip';
        if (!$archive->open($this->path.$archiveName, \ZipArchive::CREATE)){
            $this->error("Archive open error!");
            return;
        }
        $bar = $this->output->createProgressBar($countFiles);
        $i = 0;
        foreach ($files as $file) {
            $i++;
            $archive->addFile($this->path.$file, substr($file,1));
            if ($i%10==0){
                $bar->advance(10);
                $archive->close();
                $archive->open($this->path.$archiveName, \ZipArchive::CREATE);
            }
        }
        $bar->advance($i%10);
        $bar->finish();
        unlink($dbFileName);
        $this->comment("\nBackup create success!");
        $this->comment("Backup file name: ".$archiveName);
    }
}
