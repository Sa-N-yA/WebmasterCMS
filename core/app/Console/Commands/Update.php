<?php

namespace App\Console\Commands;

use App\Http\Controllers\Core\UpdateController;
use Illuminate\Console\Command;
use Symfony\Component\HttpKernel\Tests\Fragment\Bar;
use DB;
use Config;
use File;
use Artisan;

class Update extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check and install updates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        UpdateController::init();
        $version = UpdateController::checkNewVersion();
        if (!empty(UpdateController::getError())){
            $this->error(UpdateController::getError());
            return;
        }
        if ($version) {
            $this->info("New version is available for download");
            $this->info("Current version: " . UpdateController::getCurrentVersionName());
            $this->info("Last version: " . UpdateController::getCurrentVersionNameServer());
        }   else    {
            $this->info("Current version is the last");
            return;
        }
        $this->info('Start make backup');
//        Artisan::call('cms:backup');
        $this->info('Download updates');
        $lastVersion = UpdateController::getCurrentVersionServer();

        $curl = curl_init(Config::get('cms.server')."core-cms/core-cms.zip");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $res = curl_exec($curl);
        if ($res!==false){
            file_put_contents(storage_path('updates').DIRECTORY_SEPARATOR.'update.zip',$res);
            $this->info('Download archive successfully!');
        }   else    {
            $this->error('Connect failed!');
            return;
        }

        $this->info('Update successfully downloaded.');
        $archive = new \ZipArchive();
        $archive->open(storage_path('updates').DIRECTORY_SEPARATOR.'update.zip');
        $archive->extractTo(base_path().DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR);
        Artisan::call('migrate');

        $conf = Config::get('cms');
        $conf['version'] = $lastVersion;
        $conf['versionName'] = UpdateController::getCurrentVersionNameServer();
        $data = var_export($conf, true);
        if(File::put(base_path() . DIRECTORY_SEPARATOR. 'config'.DIRECTORY_SEPARATOR.'cms.php', "<?php\n return $data ;")) {
        }
        unlink(storage_path('updates').DIRECTORY_SEPARATOR.'update.zip');
        $this->comment('Updates success install');
    }
}
