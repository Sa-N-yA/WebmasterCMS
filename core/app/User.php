<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Validator;
use DB;
class User extends Authenticatable
{
    protected $table = "users";

    public function groups(){
        return $this->belongsToMany('App\Group','user_groups','user_id','group_id');
    }

    public function save(array $options = Array()){
        $validations = [
            'name' => 'required|min:3|max:255',
            'email' => 'required|email|unique:users,email'.(($this->id?','.$this->id:'')),
        ];
        if (!$this->id || $this->change_password){
            $validations['password'] = 'required|min:6|max:255|confirmed';
        }
        foreach($this->attributes as $key=>$attr){
            if (strpos($key,'group_')!==false){
                $validations[$key] = 'required|boolean';
            }
        }
        $v = Validator::make($this->attributes, $validations);
        if ($v->fails()) return $v->errors();

        DB::beginTransaction();
        if ($this->id) DB::table('user_groups')->where('user_id',$this->id)->delete();

        $groups = [];
        if (!$this->id || $this->change_password) $this->password = bcrypt($this->password);
        foreach($this->attributes as $key=>$attr){
            if (strpos($key,'group_')!==false){
                if ($attr=='1') $groups[] = str_replace('group_','',$key);
                unset($this->$key);
            }
        }
        unset($this->password_confirmation);
        unset($this->change_password);
        if (!parent::save()){
            DB::rollback();
            return false;
        }

        foreach($groups as $key=>$group){
            DB::table('user_groups')->insert(['user_id'=>$this->id, 'group_id'=>$group]);
        }



        DB::commit();
        return true;
    }
}
