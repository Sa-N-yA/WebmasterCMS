<?php
namespace App\Events;

use App\Http\Controllers\Core\CacheController;
use App\IBlockItem;



class IBlockChanged extends Event{

    public function __construct($item)
    {
        $iblock_id = $item->iblock_id;
        CacheController::deleteByTag('iblock_'.$iblock_id);
    }
}