/**
 * Created by Александр on 01.12.2016.
 */
var admin = new (function () {
    this.ajax = function (method, params, callback, error, complete) {
        params['_token'] = csrf;
        $.ajax({
            url: '/ajax/' + method,
            dataType: 'json',
            data:params,
            method:'post',
            headers: {
                'X-CSRF-TOKEN':csrf
            },
            success: function (res) {
                if (callback) callback(res);
            },
            error: function (err) {
                if (err) error(err);
            },
            complete: function () {
                if (complete) complete.call();
            }
        });
    };
    this.registerButtonAjax = function (btns, success, error) {
        var b = btns.toArray();
        for (var i = 0; i < b.length; i++) {
            (function (t) {
                t.ajaxCallbackSuccess = function (res) {
                    if (res['ERROR']) {
                        error.call(t, res);
                        return;
                    }
                    success.call(t, res)
                };
                t.ajaxCallbackError = function (res) {
                    error.call(t, res)
                };
            })(b[i]);
        }
    };
    this.registerFormAjax = function (forms, success, error) {
        var b = forms.toArray();
        console.log(b);
        for (var i = 0; i < b.length; i++) {
            (function (t) {
                t.ajaxCallbackSuccess = function (res) {
                    success.call(t, res)
                };
                t.ajaxCallbackError = function (res) {
                    error.call(t, res)
                };
            })(b[i]);
        }
    };
    var modals = {};
    this.showModal = function(id, params){
        var modal = modals[id];
        modal.modal('show');
        if (modal.onShow) modal.onShow.call(modal,params);
    };
    this.registerModal = function(id, onRegister, onShow, success, close){
        var modal = $('#modal-' + id);
        modals[id] = modal;
        if (onRegister) onRegister.call(modal);
        modal.find('.modal-close').click(function(){
            modal.modal('hide');
        });
        modal.on('hide.bs.modal',function(){
            if (close) close.call(modal);
        });
        modal.onShow = onShow;
        $('#modal-' + id).on('click','.modal-success',function(){
            if (success){
                if (!success.call(modal, this)) return;
            }
            modal.modal('hide');
        });
    };
    $(function () {
        $('.admin-btn-ajax').click(function (e) {
            e.preventDefault();
            var self = $(this);
            self.button('loading');
            var data = self.data();
            for(var key in data){
                if (!(typeof data[key] == "string" || typeof data[key]=="number")){
                    data[key] = null;
                }
            }
            admin.ajax(data.method, data, this.ajaxCallbackSuccess, this.ajaxCallbackError, function () {
                self.button('reset');
            });
            return false;
        });
        $('.admin-form-ajax').submit(function () {
            var self = this;
            var data = $(self).serializeArray();
            var d = {};
            for (var i = 0; i < data.length; i++) d[data[i].name] = data[i].value;

            $(self).find('[type=submit]').button('loading');
            admin.ajax($(self).attr('data-action'), d, function (res) {
                if (res['ERROR']){
                    for(var e in res['DATA']) {
                        var str = "";
                        for (var i = 0; i < res['DATA'][e].length; i++) {
                            str += res['DATA'][e][i];
                        }
                        if (str != "") {
                            $(self).find('#' + e).popover({
                                content: str,
                                trigger: 'manual',
                                placement: 'left'
                            }).popover('show').each(
                                function () {
                                    var self = this;
                                    setTimeout(function () {
                                        $(self).popover('destroy')
                                    }, 4000)
                                }
                            );
                            break;
                        }
                    }
                    return false;
                }
                if (self.ajaxCallbackSuccess) self.ajaxCallbackSuccess(res);
            }, this.ajaxCallbackError,
            function () {
                $(self).find('[type=submit]').button('reset');
            });
            return false;
        });
    });
    $(function(){
        if ($('#active_date').length>0){
            $('#active_date').dateRangePicker(
                {
                    separator : ' - ',
                    getValue: function()
                    {
                        if ($('input[name=active_from]').val() && $('input[name=active_to]').val() )
                            return $('input[name=active_from]').val() + ' - ' + $('input[name=active_to]').val();
                        else
                            return '';
                    },
                    setValue: function(s,s1,s2)
                    {
                        $('input[name=active_from]').val(s1);
                        $('input[name=active_to]').val(s2);
                    },
                    time: {
                        enabled: true
                    },
                    format: 'YYYY-MM-DD HH:mm:ss'
                }
            );
        }
    });
})();